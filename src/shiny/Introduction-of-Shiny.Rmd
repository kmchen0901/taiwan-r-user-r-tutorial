% Introducing Shiny: Easy web applications in R
% T.C. Hsieh
% 2013/02/25

```{r setup, include=FALSE}
# set global chunk options
opts_chunk$set(cache=TRUE)
```

Shiny介紹與教學 : 用R語言跑網頁app
=========================

簡介
-------------------------
[Shiny](http://www.rstudio.com/shiny/)第一個公開beta版本於2012年十一月釋出，截至2013年2月發布0.4.0版。整合了網頁app與[R](http://www.r-project.org/)語言計算，使得R程式開發者可以很輕易地透過瀏覽器網頁點選的方式將其程式分享給使用者 (使用者不需要懂得R語言)。Shiny支援HTML的表單輸入元件，諸如文字欄位(input)、下拉式選單(select)、多選核取方塊(checkbox)、單選核取方塊(radio)...等等。而Shiny主要的輸出格式為文字輸出 (Text output, 即R裡面的`cat`)、R格式輸出 (Printable Output, 即R裡面的`print`)、表格輸出 (Table output, 即R套件`xtable`) 以及圖片輸出 (Plot output, 即R裡面的`plot`同時支援R套件`ggplot2`)。

特色
-------------------------
- 建構一個app只要短短幾行程式碼
- 只要懂R語言即可 (不太需要HTML, CSS 或 JAVA的先備知識)
- 動態展示結果 (使用者改變輸入參數時，輸出結果也會立即改變)

開始使用
-------------------------
### 1. 安裝
```{r eval=FALSE}
install.packages('shiny')
```

### 2. Hello Shiny
Shiny的第一支例題：給定隨機樣本下生成常態分佈
```{r eval=FALSE}
library(shiny)
runExample("01_hello")
```

<iframe src="http://glimmer.rstudio.com/wush978/01_hello/" id="01_hello" frameborder="0" width="100%" height="600px"></iframe>

例題的程式檔在shiny套件的安裝目錄下：
```{r}
exDir <- paste(system.file("examples", package = "shiny"), "01_hello", sep="/")
exDir 
```

程式檔主要有兩隻，分別是ui.R(客服端設定，網頁表單基本的輸入輸出結構)以及server.R(伺服器端設定，以輸入的參數進行R程式計算並將結果傳回至客服端)。
```{r}
dir(exDir)
```
Hello Shiny的客服端設定ui.R如下所示：
```{r eval=FALSE}
#ui.R
library(shiny)

# Define UI for application that plots random distributions 
shinyUI(pageWithSidebar(
  
  # Application title
  headerPanel("Hello Shiny!"),
  
  # Sidebar with a slider input for number of observations
  sidebarPanel(
    sliderInput("obs", 
                "Number of observations:", 
                min = 0, 
                max = 1000, 
                value = 500)
  ),
  
  # Show a plot of the generated distribution
  mainPanel(
    plotOutput("distPlot")
  )
))

```
其中`shinyUI()`用來宣告本程式為客服端設定，其參數`pageWithSidebar()`乃是用來宣告具有側邊控制欄的頁面。`pageWithSidebar()`還有三個子參數分別是：`headerPanel()`用來定義網頁標題；`sidebarPanel()`用來定義左方(或是上方)的控制選單，本例中只有一個滑動輸入元件`sliderInput()`，元件id為"obs"；以及`mainPanle()`則是用來定義右方(或是下方)的輸出結果，本例中為圖片輸出元件`plotOutput()`，元件id為"distPlot"。值得一提的是，在特殊網頁設計的需求下`mainPanle()`也可以擺放輸入元件，同理`sidebarPanel()`也可擺放輸入元件。

<img src='http://dl.dropbox.com/u/26949459/Introduction%20of%20Shiny/ui_hello.png' width='60%'>

Hello Shiny的伺服端設定server.R如下所示：
```{r eval=FALSE}
#server.R
library(shiny)

# Define server logic required to generate and plot a random distribution
shinyServer(function(input, output) {
   
  # Expression that generates a plot of the distribution. The expression
  # is wrapped in a call to renderPlot to indicate that:
  #
  #  1) It is "reactive" and therefore should be automatically 
  #     re-executed when inputs change
  #  2) Its output type is a plot 
  #
  output$distPlot <- renderPlot({
        
    # generate an rnorm distribution and plot it
    dist <- rnorm(input$obs)
    hist(dist)
  })
  
})

```
其中`shinyServer()`用來宣告本程式為伺服端設定，並帶有兩個型態為list的參數：input與output。其中input$obs表示客服端自id為"obs"的原件輸入的參數；而output$distPlot表示客服端自id為"distPlot"輸出的結果。而輸入與輸出則是透過`renderPlot()`(reactive function, 反應函數)為媒介進行即時運算(改變input$obs的同時output$distPlot亦跟著改變)。

![](http://dl.dropbox.com/u/26949459/Introduction%20of%20Shiny/01_hello.png)

由此可知，Hello Shiny app的基本運作流程為：

1. 自ui.R中的輸入元件給定一個樣本數(input$obs) -> 2. 傳到server.R裡面的反應函數(renderPlot)進行計算 -> 3. 最後傳回ui.R的輸出元件(output$distPlot)畫出圖片。

### 3. 更複雜的例子

```{r eval=FALSE}
library(shiny)
runExample("03_reactivity")
```

<iframe src="http://glimmer.rstudio.com/wush978/03_reactivity/" id="03_reactivity" frameborder="0" width="100%" height="600px"></iframe>

ui.R
```{r eval=FALSE}
library(shiny)

# Define UI for dataset viewer application
shinyUI(pageWithSidebar(
  
  # Application title
  headerPanel("Reactivity"),
  
  # Sidebar with controls to provide a caption, select a dataset, and 
  # specify the number of observations to view. Note that changes made
  # to the caption in the textInput control are updated in the output
  # area immediately as you type
  sidebarPanel(
    textInput("caption", "Caption:", "Data Summary"),
    
    selectInput("dataset", "Choose a dataset:", 
                choices = c("rock", "pressure", "cars")),
    
    numericInput("obs", "Number of observations to view:", 10)
  ),
  
  
  # Show the caption, a summary of the dataset and an HTML table with
  # the requested number of observations
  mainPanel(
    h3(textOutput("caption")), 
    
    verbatimTextOutput("summary"), 
    
    tableOutput("view")
  )
))

```
在這個例題裡面`sidebarPanel()`包含了三個input元件。就元件名稱的定義不難理解`textInput()`是單行文字輸入元件(多行輸入要用textarea但Shiny 0.4.0沒有直接可用的API，要額外透過HTML/CSS的幫助來調用)，`selectInput()`是下拉式選單(預設是單選選單)，而`numericInput()`是數值輸入元件(可以是浮點數)。在`mainPanel()`裡面也包含了三個output元件。`textOutput()`是單純的文字輸出模式，通常對應server.R裡面的`renderText()`進行輸出，`verbatimTextOutput()`是逐字文本輸出模式，通常對應server.R裡面的`renderPrint()`用來輸出R的output，而`tableOutput()`是表格輸出模式，對應server.R裡面的`renderTable()`將R的dataframe以xtable以HTML輸出。

server.R
```{r eval=FALSE}
library(shiny)
library(datasets)

# Define server logic required to summarize and view the selected dataset
shinyServer(function(input, output) {

  # By declaring databaseInput as a reactive expression we ensure that:
  #
  #  1) It is only called when the inputs it depends on changes
  #  2) The computation and result are shared by all the callers (it 
  #     only executes a single time)
  #  3) When the inputs change and the expression is re-executed, the
  #     new result is compared to the previous result; if the two are
  #     identical, then the callers are not notified
  #
  datasetInput <- reactive({
    switch(input$dataset,
           "rock" = rock,
           "pressure" = pressure,
           "cars" = cars)
  })
  
  # The output$caption is computed based on a reactive expression that
  # returns input$caption. When the user changes the "caption" field:
  #
  #  1) This function is automatically called to recompute the output 
  #  2) The new caption is pushed back to the browser for re-display
  # 
  # Note that because the data-oriented reactive expressions below don't 
  # depend on input$caption, those expressions are NOT called when 
  # input$caption changes.
  output$caption <- renderText({
    input$caption
  })
  
  # The output$summary depends on the datasetInput reactive expression, 
  # so will be re-executed whenever datasetInput is re-executed 
  # (i.e. whenever the input$dataset changes)
  output$summary <- renderPrint({
    dataset <- datasetInput()
    summary(dataset)
  })
  
  # The output$view depends on both the databaseInput reactive expression
  # and input$obs, so will be re-executed whenever input$dataset or 
  # input$obs is changed. 
  output$view <- renderTable({
    head(datasetInput(), n = input$obs)
  })
})
```
在本例中最值得一提的是`reactive()`函式，它是用來決定下拉式選單中 (input$dataset) 選擇了哪一筆dataset，並從R的workspace中選取該筆資料集。除了利用`switch()`來選擇dataset，也可以用`get()`，給一個例子：
```{r eval=FALSE}
datasetInput <- reactive({
    get(input$dataset)
  })
```

### 4. 寫一隻自己的app
開啟一個空的資料夾，並建立ui.R與server.R兩個新檔，譬如：
```
~/shinyapp
|-- ui.R
|-- server.R
```
接著仿造前面的例題編寫ui.R與server.R。以前一個例題為例，請記得`textInput()`與`selectInput()`都只是`sidebarPanel()`的參數，所以參數之間要以逗號(,)做分隔，同理其他函式也以此類推。完成之後在R裡面執行以下指令即可在自己電腦的瀏覽器上操作app。
```{r eval=FALSE}
library(shiny)
runApp("~/shinyapp")
```

### 5. 在網際網路上執行app
利用Shiny製作完成app之後，應當不滿足只能在local端執行吧，好比以下幾個例子
* [Stocks](http://glimmer.rstudio.com/winston/stocks/)
* [iNEXT](http://glimmer.rstudio.com/tchsieh/inext/)
* [Marketing](http://vnijs.rady.ucsd.edu:3838/marketing/)
* [Reconstruct Gene Networks](http://glimmer.rstudio.com/qbrc/grn/)
* [Scatter chart](http://glimmer.rstudio.com/mages/Example_1/)

要達到這樣的效果基本上需要一台Linux server來跑[Shiny server](https://github.com/rstudio/shiny-server)。比較簡單的方式是把source code上傳到目前免費的Rstudio的[glimmer server](https://rstudio.wufoo.com/forms/shiny-server-beta-program/)上執行。

### 6. 進階功能 (動態UI)
####  a). conditionalPanel
當客服端頁面中某些控制輸入(或輸出)選項(或是子頁面, tabPanel) 想要具有隱藏或出現功能時可以在ui.R中使用conditionalPanel，考慮一個隨機生成常態分佈或是T分布的程式，由於兩種分布的參數不同，我們可以透過conditionalPanel來控制參數的輸入元件，程式碼如下所示：值得一提的是conditionalPanel裡面的condition參數設定方式必須為"input.元件id == 元件值"，當元件值為logical時要用小寫表示(假設為TRUE)，即用"input.元件id == true"。

ui.R
```{r eval=FALSE}
library(shiny)
shinyUI(pageWithSidebar(
  headerPanel("Generating distribution"),
  sidebarPanel(
    selectInput(inputId="method", 
                label="Choose distribution:", 
                choices=c("Normal"="norm", "Student t"="st")),
    helpText("Setting parameter(s) for distribution model"),
    conditionalPanel(condition="input.method=='norm'",
                     numericInput(inputId="mu", label="mean", value=0),
                     numericInput(inputId="sd", label="standard deviation", value=1, min=0)
    ),
    conditionalPanel(condition="input.method=='st'",
                     numericInput(inputId="df", label="Df", value=10, min=1)
    ),
    sliderInput(inputId="obs", 
                label="Number of observations:", 
                min = 0, max = 1000, value = 500)
  ),
  mainPanel(
    plotOutput("distPlot")
  )
  
))
```

server.R
```{r eval=FALSE}
library(shiny)
shinyServer(function(input, output) {
  output$distPlot <- renderPlot({
    if(input$method == "norm") dist <- rnorm(input$obs, mean=input$mu, sd=input$sd)  
    if(input$method == "st") dist <- rt(input$obs, df=input$df)  
    hist(dist)
  })  
})
```
<iframe src="http://glimmer.rstudio.com/wush978/04_gen_distribution/" id="04_gen_distribution" frameborder="0" width="100%" height="600px"></iframe>

####  b). renderUI and uiOutput
當某些輸入元件會受到上一層輸入設定所影響時，可以使用renderUI與uiOutput。下面的例子提到第一層控制選單`selectInput()`先選定某一筆dataset，然後再由第二層`uiOutput()`列被選定資料集的colnum names，最後再核取選定的colnum names輸出前20筆資料到到`mainPanel()`

ui.R
```{r eval=FALSE}
shinyUI(pageWithSidebar(
  headerPanel("renderUI and uiOutput"),
  
  sidebarPanel(
    selectInput("dataset", "Data set", as.list(c("mtcars", "morley", "rock"))),
    uiOutput("choose_columns")
  ),
  mainPanel(
    tableOutput("data_table")
  )  
))
```

server.R
```{r eval=FALSE}
shinyServer(function(input, output) {
  output$choose_columns <- renderUI({
    # If missing input, return to avoid error later in function
    if(is.null(input$dataset))
      return()

    # Get the data set with the appropriate name
    dat <- get(input$dataset)
    colnames <- names(dat)
 
    # Create the checkboxes and select them all by default
    checkboxGroupInput("columns", "Choose columns", 
                        choices  = colnames,
                        selected = colnames)
  })
 
  # Output the data
  output$data_table <- renderTable({
    # If missing input, return to avoid error later in function
    if(is.null(input$dataset))
      return()
 
    # Get the data set
    dat <- get(input$dataset)
 
    # Make sure columns are correct for data set (when data set changes, the
    # columns will initially be for the previous data set)
    if (is.null(input$columns) || !(input$columns %in% names(dat)))
      return()
 
    # Keep the selected columns
    dat <- dat[, input$columns, drop = FALSE]
 
    # Return first 20 rows
    head(dat, 20)
  })
})
```

<iframe src="http://glimmer.rstudio.com/wush978/05_renderUI/" id="05_renderUI" frameborder="0" width="100%" height="600px"></iframe>

## 相關學習資源
* [Shiny官方教學網站](http://rstudio.github.com/shiny/tutorial/)
* [Shiny官方討論區](https://groups.google.com/forum/?fromgroups#!forum/shiny-discuss)
