% googleVis
% Taiwan R User Group
% 2013-02-18

# [Google Chart Tools][GoogleChartTool]

- Google Visualization API
- Use _JavaScript_ and Data Table(in _JSON_) as input
- Output is either _HTML5_/_SVG_ or _Flash_
- Browser with internet connection is required
- Please Read [Terms of Service](https://developers.google.com/terms/)

# googleVis

- Development started in Auguest 2010
- [googleVis][googleVis] is a package for [R][R], see [@Markus2011]
- Allow user to visualize data with [Google Chart Tools][GoogleChartTool] without
manually upload data to Google

# Key Ideas

- R function generate html files with references to [Google Chart Tools][GoogleChartTool]
- Transform related data to JSON

# googleVis 0.3.3

```{r gvis-table, results='asis', echo=FALSE}
suppressPackageStartupMessages(require(xtable))
suppressPackageStartupMessages(require(gdata))
gvis.table <- read.xls("gvis.xls", header=FALSE)
names(gvis.table) <- c("function", "comments")
print(xtable(gvis.table), type="html")
```

# gvisAnnotatedTimeLine

```{r gvisAnnotatedTimeLine, results='asis', message=FALSE}
library(googleVis)
data(Stock)
A1 <- gvisAnnotatedTimeLine(Stock, datevar="Date",
                           numvar="Value", idvar="Device",
                           titlevar="Title", annotationvar="Annotation",
                           options=list(displayAnnotations=TRUE,
                            legendPosition='newRow',
                            width=600, height=350)
                           )
plot(A1, "chart")
```

# gvisAreaChart

```{r gvisAreaChart, results='asis'}
## Please note that by default the googleVis plot command
## will open a browser window and requires an internet
## connection to display the visualisation.

df=data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))

## Area chart
Area1 <- gvisAreaChart(df, xvar="country", yvar=c("val1", "val2"))
plot(Area1, "chart")
```

# gvisBarChart

```{r gvisBarChart, results='asis'}
df=data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))

## Bar chart
Bar1 <- gvisBarChart(df, xvar="country", yvar=c("val1", "val2"))
plot(Bar1, "chart")
```

# gvisBubbleChart

```{r gvisBubbleChart, results='asis'}
bubble1 <- gvisBubbleChart(Fruits, idvar="Fruit", xvar="Sales", yvar="Expenses")
plot(bubble1, "chart")
```

# gvisCandlestickChart

```{r gvisCandlestickChart, results='asis'}
C1 <- gvisCandlestickChart(OpenClose, xvar="Weekday", low="Low",
                                      open="Open", close="Close",
                                      high="High",
                                      options=list(legend='none'))
plot(C1, "chart")
```

# gvisColumnChart

```{r gvisColumnChart, results='asis'}
df=data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))

## Column chart
Col1 <- gvisColumnChart(df, xvar="country", yvar=c("val1", "val2"))
plot(Col1, "chart")
```

# gvisComboChart

```{r gvisComboChart, results='asis'}
## Add the mean
CityPopularity$Mean=mean(CityPopularity$Popularity)

C1 <- gvisComboChart(CityPopularity, xvar="City",
                                     yvar=c("Mean", "Popularity"),
                                   options=list(seriesType="bars",
                                                title="City Popularity",
                                                series='{0: {type:"line"}}'))
plot(C1, "chart")
```

# gvisGauge

```{r gvisGauge, results='asis'}
Gauge1 <- gvisGauge(CityPopularity, options=list(min=0, max=800, greenFrom=500,
                    greenTo=800, yellowFrom=300, yellowTo=500,
                    redFrom=0, redTo=300))

plot(Gauge1, "chart")
```

# gvisGeoChart

```{r gvisGeoChart, results='asis'}
G1 <- gvisGeoChart(Exports, locationvar='Country', colorvar='Profit') 

plot(G1, "chart")
```

# gvisGeoMap

```{r gvisGeoMap, results='asis'}

G1 <- gvisGeoMap(Exports, locationvar='Country', numvar='Profit',
                 options=list(dataMode="regions")) 

plot(G1, "chart")
```

# gvisIntensityMap

```{r gvisIntensityMap, results='asis'}
df=data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))
Intensity1 <- gvisIntensityMap(df, locationvar="country", numvar=c("val1", "val2"))
plot(Intensity1, "chart")
```

# gvisLineChart

```{r gvisLineChart, results='asis'}
df <- data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))

## Line chart
Line1 <- gvisLineChart(df, xvar="country", yvar=c("val1", "val2"))
plot(Line1, "chart")
```

# gvisMap

```{r gvisMap, results='asis'}
data(Andrew)

M1 <- gvisMap(Andrew, "LatLong" , "Tip",
              options=list(showTip=TRUE, showLine=TRUE, enableScrollWheel=TRUE,
                           mapType='hybrid', useMapTypeControl=TRUE,
                           width=800,height=400))

plot(M1, "chart") 
```

# gvisMerge

```{r gvisMerge, results='asis'}
G <- gvisGeoChart(Exports, "Country", "Profit", 
                  options=list(width=200, height=100))
T <- gvisTable(Exports, 
                  options=list(width=200, height=260))

GT <- gvisMerge(G,T, horizontal=FALSE) 
plot(GT, "chart")
```

# gvisMotionChart

```{r gvisMotionChart, results='asis'}
## timevar Year
M1 <- gvisMotionChart(Fruits, idvar="Fruit", timevar="Year")
plot(M1, "chart")
```

# gvisOrgChart

```{r gvisOrgChart, results='asis'}
Org1 <- gvisOrgChart(Regions, idvar = "Region", parentvar = "Parent", 
     			      tipvar="Val")
plot(Org1, "chart")
```

# gvisPieChart

```{r gvisPieChart, results='asis'}
Pie1 <- gvisPieChart(CityPopularity)
plot(Pie1, "chart")
```

# gvisScatterChart

```{r gvisScatterChart, results='asis'}
Scatter1 <- gvisScatterChart(women)
plot(Scatter1, "chart")
```

# gvisSteppedAreaChart

```{r gvisSteppedAreaChart, results='asis'}
df=data.frame(country=c("US", "GB", "BR"), val1=c(1,3,4), val2=c(23,12,32))

## Stepped Area chart
SteppedArea1 <- gvisSteppedAreaChart(df, xvar="country", yvar=c("val1", "val2"))
plot(SteppedArea1, "chart")
```

# gvisTable

```{r gvisTable, results='asis'}
tbl1 <- gvisTable(Population)
plot(tbl1, "chart")
```


# gvisTreeMap

```{r gvisTreeMap, results='asis'}
require(datasets)
states <- data.frame(state.name, state.area)

## Create parent variable

total=data.frame(state.area=sum(states$state.area), state.name="USA")

my.states <- rbind(total, states)
my.states$parent="USA"
## Set parent variable to NA at root level
my.states$parent[my.states$state.name=="USA"] <- NA

my.states$state.area.log=log(my.states$state.area)
statesTree <- gvisTreeMap(my.states, "state.name", "parent",
                          "state.area", "state.area.log")
plot(statesTree, "chart")
```

# Reference

[Interactive charts and slides with R, googleVis and knitr](http://dl.dropbox.com/u/7586336/blogger/Cambridge_R_googleVis_with_knitr_and_RStudio_May_2012.html)

[googleVis]: http://code.google.com/p/google-motion-charts-with-r/
[R]: http://www.r-poject.org/
[GoogleChartTool]: https://developers.google.com/chart/